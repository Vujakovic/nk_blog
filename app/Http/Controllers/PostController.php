<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Tag;
use App\Models\PostTag;

class PostController extends Controller
{

    public $tagArray=array();

    public function index()
    {
        DB::listen(function ($query) {
            info($query->sql);
        });

        $posts = Post::with('comments', 'author')->published()->paginate(10);

        return view('posts.index', compact('posts')); // [ 'posts' => $posts]
    }

    public function show($id)
    {
        // $post = Post::where('id', $id)->first();
        $post = Post::findOrFail($id);
        $post->load(['comments.user']);
        
        return view('posts.show', compact('post')); // [ 'post' => $post]
    }

    public function create(Request $request)
    {
        $tags = Tag::all();
        return view('posts.create', compact('tags'));
    }

    public function store(StorePostRequest $request)
    {
        DB::listen(function ($query) {
            info($query->sql);
        });

        // $post = new Post();
        // $post->title = $request->get('title', '');
        // $post->body = $request->get('body', '');
        // $post->is_published = $request->get('is_published', false);
        // $post->save();

        $data = $request->validated();

        $post = Auth::user()->posts()->create($data);
        
        $post->tags()->sync($data['tags']);

        // return view('post', compact('post'));
        return redirect("/posts/$post->id");
    }
}
